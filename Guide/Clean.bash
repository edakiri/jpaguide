#!/usr/bin/bash
set -e
for D in *
do

	if [[ ! -d "$D" ]]
	then
		continue
	fi

	pushd "$D" >>/dev/null
	xargs '--delimiter=\n' --max-args=1 --arg-file=.gitignore rm -rf
	popd >>/dev/null

done
