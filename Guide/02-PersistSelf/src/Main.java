/* Persist Self */
import javax.persistence.*;

@Entity
public class Main //supposed to implement Serializable, but it works without
{

    public static void main(String[] arguments) {
	EntityManagerFactory entityManagerFactory
	    = Persistence.createEntityManagerFactory("PersistenceUnit");
	EntityManager entityManager
	    = entityManagerFactory.createEntityManager();
	EntityTransaction transaction = entityManager.getTransaction();
// An EntityTransaction instance can be reused.
	transaction.begin();
	char character = 'A';
	for (byte i = 0; i < 3; ++i) {
	    Main single = new Main();
	    single.setText(character++);
	    entityManager.persist(single);
	}
	transaction.commit();
    }

    @Id // An @Id alone is not enough for EclipseLink. There must be an @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY) //AUTO does not work
    private byte which;

    @Column
    char text;

    public void setText(char text) {
	this.text = text;
    }

    @Override
    public String toString() {
	return String.valueOf(which) + ' ' + text;
    }
}
