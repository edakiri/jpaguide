/* Persist Other */
import java.util.List;
import javax.persistence.*;
import javax.persistence.criteria.*;

public class Main {

    public static void main(String[] arguments) {
	EntityManagerFactory entityManagerFactory
	    = Persistence.createEntityManagerFactory("PersistenceUnit");
	EntityManager entityManager
	    = entityManagerFactory.createEntityManager();
	EntityTransaction transaction = entityManager.getTransaction();
// An EntityTransaction instance can be reused.
	transaction.begin();
	char character = 'A';
	for (byte i = 0; i < 3; ++i) {
	    Many single = new Many();
	    single.setWhich(i);//Order is guaranteed if we set the @Id value
	    single.setText(character++);
	    entityManager.persist(single);
	}
	transaction.commit();

	CriteriaBuilder builder = entityManager.getCriteriaBuilder();
	CriteriaQuery<Many> criteriaQuery = builder.createQuery(Many.class);
	Root<Many> many = criteriaQuery.from(Many.class);
	criteriaQuery.select(many);
	TypedQuery<Many> query = entityManager.createQuery(criteriaQuery);
	List<Many> all = query.getResultList();
	for (Many single : all) System.out.println(single);
    }
}
