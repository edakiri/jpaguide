/* Persist Other */
import javax.persistence.*;

@Entity //supposed to implement Serializable, but it works without
public class Many
{
    @Id // An @Id alone is not enough for EclipseLink. There must be an @Column
//@GeneratedValue(strategy = GenerationType.IDENTITY) //AUTO does not work
    private byte which;

    @Column
    char text;

    public void setText(char text) {
	this.text = text;
    }

    public void setWhich(byte which) {
	this.which = which;
    }

    @Override
    public String toString() {
	return String.valueOf(which) + ' ' + text;
    }
}
