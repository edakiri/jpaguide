/* Initialize JPA */
import javax.persistence.*;

public class Main {

    public static void main(String[] arguments) {
	EntityManagerFactory entityManagerFactory
	    = Persistence.createEntityManagerFactory("PersistenceUnit");
	EntityManager entityManager
	    = entityManagerFactory.createEntityManager();
	EntityTransaction transaction = entityManager.getTransaction();
// The EntityTransaction instance can be reused.
	transaction.begin();
	transaction.commit();
    }
}
