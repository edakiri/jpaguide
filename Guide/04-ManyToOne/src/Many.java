/* Persist Other */
import javax.persistence.*;

@Entity //supposed to implement Serializable, but it works without
public class Many {

    @Id // An @Id alone is not enough for EclipseLink. There must be an @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY) //AUTO does not work
    private byte which;

    @Column
    char text;

    @ManyToOne
    One one;

    public One getOne() {
	return one;
    }

    public void setOne(One one) {
	this.one = one;
    }

    public void setText(char text) {
	this.text = text;
    }

    @Override
    public String toString() {
	return one.toString() + ' ' + text;
    }
}
