/* Persist Other */
import javax.persistence.*;

@Entity //supposed to implement Serializable, but it works without
public class One {

    @Id // An @Id alone is not enough for EclipseLink. There must be an @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY) //AUTO does not work
    private byte which;

    @Column
    private byte value;

    public void set(byte value) {
	this.value = value;
    }

    @Override
    public String toString() {
	return String.valueOf(value);
    }
}
